package br.com.platformbuilder.infrastructure.repository.specification;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.jpa.domain.Specification;

import br.com.platformbuilder.domain.model.Cliente;

public class ClienteSpecification {
	
	private String nome;
	private String cpf;
	
	public ClienteSpecification(String nome, String cpf) {
		this.nome = nome;
		this.cpf = cpf;
	}
	
	private Specification<Cliente> nome(String nome) {
		return (root, query, builder) -> builder.like(root.get("nome"), "%" + nome + "%");
	}
	
	private Specification<Cliente> cpf(String cpf) {
		return (root, query, builder) -> builder.like(root.get("cpf"), "%" + cpf + "%");
	}
	
	public Specification<Cliente> build() {

		Specification<Cliente> where = Specification.where(null);

		if (!StringUtils.isEmpty(nome)) {
			where = Specification.where(where).and(nome(this.nome));
		}

		if (!StringUtils.isEmpty(cpf)) {
			where = Specification.where(where).and(cpf(this.cpf));
		}

		return where;
	}
}
