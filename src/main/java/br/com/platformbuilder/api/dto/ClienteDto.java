package br.com.platformbuilder.api.dto;

import java.time.LocalDateTime;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ClienteDto {
	private Integer id;
	
	@NotBlank
	private String nome;
	
	@NotBlank
	@Size(max = 11, min = 11, message = "Deve ter 11 dígitos")
	private String cpf;
	
	@NotNull
	private LocalDateTime nascimento;
	
	private int idade;
}
