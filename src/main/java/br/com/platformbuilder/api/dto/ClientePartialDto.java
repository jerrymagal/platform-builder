package br.com.platformbuilder.api.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ClientePartialDto {
	
	@NotBlank
	@Size(max = 11, min = 11, message = "Deve ter 11 dígitos")
	private String cpf;
}
