package br.com.platformbuilder.api.converter;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.platformbuilder.api.dto.ClienteDto;
import br.com.platformbuilder.domain.model.Cliente;

@Component
public class ClienteConverter {
	
	@Autowired
	private ModelMapper modelMapper;
	
	public ClienteDto toDto(Cliente cliente) {
		ClienteDto dto = modelMapper.map(cliente, ClienteDto.class);
		dto.setIdade(calcularIdade(cliente.getDataNascimento()));
		return dto;
	}

	public Cliente toModel(ClienteDto dto) {
		Cliente cliente = modelMapper.map(dto, Cliente.class);
		cliente.setDataNascimento(dto.getNascimento());
		return cliente;
	}
	
	private Integer calcularIdade(LocalDateTime dataNascimento) {
		LocalDate hoje = LocalDate.now();
		Period periodo = Period.between(dataNascimento.toLocalDate(), hoje);
		return periodo.getYears();
	}

	public List<ClienteDto> toCollectionDto(List<Cliente> clientes) {
		return clientes.stream()
				.map(cozinha -> toDto(cozinha))
				.collect(Collectors.toList());
	}
	
	public void copyObject(ClienteDto dto, Cliente cliente) {
		modelMapper.map(dto, cliente);
		cliente.setDataNascimento(dto.getNascimento());
	}
}
