package br.com.platformbuilder.api.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.platformbuilder.api.dto.ClienteDto;
import br.com.platformbuilder.api.dto.ClientePartialDto;
import br.com.platformbuilder.domain.exception.EntidadeNaoEncontradaException;
import br.com.platformbuilder.domain.service.ClienteService;

@RestController
@RequestMapping("/cliente")
public class ClienteController {
	
	@Autowired
	private ClienteService service;
	
	@GetMapping
	public Page<ClienteDto> listar(@RequestParam(required = false) String nome, 
								   @RequestParam(required = false) String cpf,
								   @PageableDefault(size = 10) Pageable pageable) {
		return service.listar(nome, cpf, pageable);
	}
	
	@PostMapping
	public ResponseEntity<ClienteDto> incluir(@Valid @RequestBody ClienteDto dto) {
		ClienteDto clienteDto = service.incluir(dto);
		return ResponseEntity.status(HttpStatus.CREATED).body(clienteDto);
	}
	
	@PutMapping("/{id}")
	@ResponseStatus(HttpStatus.OK)
	public void alterar(@PathVariable Integer id, @Valid @RequestBody ClienteDto dto) throws EntidadeNaoEncontradaException {
		service.alterar(id, dto);
	}
	
	@PatchMapping("/{id}")
	@ResponseStatus(HttpStatus.OK)
	public void alterarDataNascimento(@PathVariable Integer id, @Valid @RequestBody(required = true) ClientePartialDto dto) throws EntidadeNaoEncontradaException {
		service.alterarCpf(id, dto.getCpf());
	}
	
	@DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.OK)
	public void deletar(@PathVariable Integer id) throws EntidadeNaoEncontradaException {
		service.excluir(id);
	}
}
