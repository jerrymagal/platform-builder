package br.com.platformbuilder.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.platformbuilder.domain.model.Cliente;
import br.com.platformbuilder.infrastructure.repository.JpaSpecificationExecutorProjection;

@Repository
public interface ClienteRepository extends JpaRepository<Cliente, Integer>, JpaSpecificationExecutorProjection<Cliente> {
}
