package br.com.platformbuilder.domain.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.platformbuilder.api.converter.ClienteConverter;
import br.com.platformbuilder.api.dto.ClienteDto;
import br.com.platformbuilder.domain.exception.EntidadeNaoEncontradaException;
import br.com.platformbuilder.domain.model.Cliente;
import br.com.platformbuilder.domain.repository.ClienteRepository;
import br.com.platformbuilder.infrastructure.repository.specification.ClienteSpecification;

@Service
public class ClienteService {
	
	@Autowired
	private ClienteConverter converter;
	
	@Autowired
	private ClienteRepository repository;
	
	public Page<ClienteDto> listar(String nome, String cpf, Pageable pageable) {
		ClienteSpecification specification = new ClienteSpecification(nome, cpf);
		Page<Cliente> clientes = repository.findAll(specification.build(), pageable);
		List<ClienteDto> dtoList = converter.toCollectionDto(clientes.getContent());
		return new PageImpl<>(dtoList, pageable, clientes.getTotalElements());
	}
	
	@Transactional
	public ClienteDto incluir(ClienteDto dto) {
		Cliente cliente = repository.save(converter.toModel(dto));
		return converter.toDto(cliente);
	}
	
	@Transactional
	public void alterar(Integer id, ClienteDto dto) throws EntidadeNaoEncontradaException {
		Optional<Cliente> optional = repository.findById(id);
		if(optional.isEmpty()) throw new EntidadeNaoEncontradaException("Cliente não encontrado");
		
		Cliente cliente = optional.get();
		converter.copyObject(dto, cliente);
		
		repository.save(cliente);
	}
	
	@Transactional
	public void alterarCpf(Integer id, String cpf) throws EntidadeNaoEncontradaException {
		Optional<Cliente> optional = repository.findById(id);
		if(optional.isEmpty()) throw new EntidadeNaoEncontradaException("Cliente não encontrado");
		
		Cliente cliente = optional.get();
		cliente.setCpf(cpf);
		
		repository.save(cliente);
	}
	
	@Transactional
	public void excluir(Integer id) throws EntidadeNaoEncontradaException {
		Optional<Cliente> optional = repository.findById(id);
		if(optional.isEmpty()) throw new EntidadeNaoEncontradaException("Cliente não encontrado");
		repository.delete(optional.get());
	}
}
