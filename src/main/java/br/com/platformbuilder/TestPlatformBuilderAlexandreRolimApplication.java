package br.com.platformbuilder;

import org.modelmapper.ModelMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import br.com.platformbuilder.infrastructure.repository.impl.JpaSpecificationExecutorProjectionImpl;

@SpringBootApplication
@EnableJpaRepositories(repositoryBaseClass=JpaSpecificationExecutorProjectionImpl.class)
public class TestPlatformBuilderAlexandreRolimApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestPlatformBuilderAlexandreRolimApplication.class, args);
	}
	
	@Bean
	public ModelMapper modelMapper() {
		return new ModelMapper();
	}
}
