CREATE TABLE cliente (
    id integer not null auto_increment,
    nome varchar(200) not null,
    cpf varchar(11) not null,
    data_nascimento datetime not null,
    primary key (id)
) engine=InnoDB default charset=utf8;


INSERT INTO cliente (nome, cpf, data_nascimento) VALUES ('Alexandre Rolim', '09233360792', '1982-06-24');